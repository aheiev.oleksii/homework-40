let arr = [1, 2, 3, 4, [5, 6, 7, 8, [9, 10, 11, 12, [13, 14, 15, 16]]], 17, 18];
const list = document.querySelector('#list');

function renderList(path, arr) {
    const ul = document.createElement('ul');
    let li;
    path.append(ul);
    arr.forEach(el => {
        if (Array.isArray(el)) {
            renderList(li, el);
            return;
        } else {
            li = document.createElement('li');
            li.innerText = el;
            ul.append(li);
        }
    });
}

renderList(list, arr);